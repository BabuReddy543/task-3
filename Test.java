import java.util.Iterator;
import java.util.TreeSet;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet<Employee> ts = new TreeSet<Employee>();     
		ts.add(new Employee("babu",1));
		ts.add(new Employee("sunny",5));
		ts.add(new Employee("ramu",3));
		ts.add(new Employee("raju",4));
		ts.add(new Employee("ramesh",2));   
	    Iterator<Employee> itr = ts.iterator();
	 
	 while(itr.hasNext()){
	 
	     Employee obj= (Employee)itr.next();
	     System.out.println(obj.getEmp_name()+"  "+obj.getEmp_id()); 

	  }         
	}

}
